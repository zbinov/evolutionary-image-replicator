# Evolutionary Image Replicator

This project is an example usage of an evolutionary algorithm and an attempt to rewrite Roger Johansson's EvoLisa program while achieving following goals:
- up-to-date technologies
- multi-platform availability
- simple graphical user interface giving the user control over algorithm's parameters during runtime
- open-source based

This has been done with:
- .NET 5
- Avalonia UI

## Presentation

Process of evolutionary image replication:

<div align="center">
  <img src="docs/lisa_source.jpg" width="45%" />
  <img src="docs/lisa_evolution.gif" width="45%" />
</div>

<div align="center">
  <img src="docs/lena_bw_source.png" width="45%" />
  <img src="docs/lena_bw_evolution.gif" width="45%" />
</div>

Application window:

<div align="center">
  <img src="docs/application_0.jpg" />
</div>

## Sources

### Idea, discussions and legacy code

- https://rogerjohansson.blog/2008/12/07/genetic-programming-evolution-of-mona-lisa/
- https://rogerjohansson.blog/2008/12/23/clustering-evolution-the-failures-in-evolisa/
- https://rogerjohansson.blog/2008/12/23/hill-climbing-gp-ga-ep-es/
- https://rogerjohansson.blog/2008/12/12/evolutionary-misconceptions/
- https://rogerjohansson.blog/2009/01/01/clustering-evolution-we-have-lift-off/
- https://rogerjohansson.blog/2009/01/04/scaling-clustere-evolution-1-1-4/
- https://code.google.com/archive/p/alsing/downloads
- https://danbystrom.se/2008/12/14/improving-performance/
- https://web.archive.org/web/20090305152406/http://www.screamingduck.com/Article.php?ArticleID=46&Show=ABCE

### Tech stack

- https://dotnet.microsoft.com
- https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-5
- https://avaloniaui.net/
