﻿using Avalonia;
using Avalonia.Media.Imaging;
using Gui.Adapters;
using MessageBox.Avalonia.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Gui.ViewModels
{
    internal class MainWindowViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;

        private void PropertyChangedInvoke([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private IFileChooser FileChooser { get; }
        internal Settings Settings { get; }

        #region ui settings
        private Boolean _startButtonActive;
        internal Boolean StartButtonActive
        {
            get => _startButtonActive;
            set
            {
                _startButtonActive = value;
                PropertyChangedInvoke();
            }
        }

        private Boolean _stopButtonActive;
        internal Boolean StopButtonActive
        {
            get => _stopButtonActive;
            set
            {
                _stopButtonActive = value;
                PropertyChangedInvoke();
            }
        }

        private Boolean _filepathSourceButtonActive = true;
        internal Boolean FilepathSourceButtonActive
        {
            get => _filepathSourceButtonActive;
            set
            {
                _filepathSourceButtonActive = value;
                PropertyChangedInvoke();
            }
        }

        private Boolean _filepathResultButtonActive;
        internal Boolean FilepathResultButtonActive
        {
            get => _filepathResultButtonActive;
            set
            {
                _filepathResultButtonActive = value;
                PropertyChangedInvoke();
            }
        }

        private Boolean _settingsSidebarActive;
        internal Boolean SettingsSidebarActive
        {
            get => _settingsSidebarActive;
            set
            {
                _settingsSidebarActive = value;
                PropertyChangedInvoke();
            }
        }

        internal List<Point> Arrow
        {
            get
            {
                if (SettingsSidebarActive)
                {
                    return new List<Point>()
                    {
                        new Point(0, 0),
                        new Point(4, 4),
                        new Point(0, 8)
                    };
                }
                else
                {
                    return new List<Point>()
                    {
                        new Point(4, 0),
                        new Point(0, 4),
                        new Point(4, 8)
                    };
                }
            }
        }
        #endregion ui settings

        internal string FilepathSource
        {
            get => ArtAlg.Settings.FilepathSource;
            set
            {
                ArtAlg.Settings.FilepathSource = value;
                PropertyChangedInvoke();
            }
        }

        internal static ulong Generation => ArtAlg.Program.Generation;
        internal static Bitmap Image
        {
            get
            {
                using var bitmap = ArtAlg.Program.Bitmap;
                using var memoryStream = new MemoryStream();

                bitmap.Save(memoryStream, ImageFormat.Bmp);
                memoryStream.Position = 0;

                return new Bitmap(memoryStream);
            }
        }
        internal static int PolygonCount => ArtAlg.Program.PolygonCount;
        internal static int PointCount => ArtAlg.Program.PointCount;
        internal static string MeanPixelError
        {
            get
            {
                var result = ArtAlg.Program.MeanPixelError?.ToString("0.00000");
                return string.IsNullOrWhiteSpace(result) ? "0" : result;
            }
        }
        internal static string PSNR
        {
            get
            {
                var result = ArtAlg.Program.PSNR?.ToString("0.00000");
                return string.IsNullOrWhiteSpace(result) ? "0" : result;
            }
        }
        internal static string SSIM
        {
            get
            {
                var result = ArtAlg.Program.SSIM?.ToString("0.00000");
                return string.IsNullOrWhiteSpace(result) ? "0" : result;
            }
        }

        internal static string Timer
        {
            get
            {
                var timeSpan = ArtAlg.Program.stopwatch.Elapsed;
                return string.Format(
                    "{0:00}:{1:00}:{2:00}.{3:00}",
                    timeSpan.Hours,
                    timeSpan.Minutes,
                    timeSpan.Seconds,
                    timeSpan.Milliseconds / 10
                );
            }
        }

        public MainWindowViewModel(IFileChooser fileChooser)
        {
            FileChooser = fileChooser;
            Settings = new Settings();
        }

        public async Task LoadImage()
        {
            var path = await FileChooser.OpenFileDialog().ConfigureAwait(false);

            if (!string.IsNullOrWhiteSpace(path))
            {
                FilepathSource = path;
                StartButtonActive = true;
            }
        }

        public async Task SaveImage()
        {
            var path = await FileChooser.SaveFileDialog().ConfigureAwait(false);

            try
            {
                using var bitmap = ArtAlg.Program.Bitmap;
                bitmap.Save(path + ArtAlg.Settings.FileExtension);
            }
            catch
            {
                var messageBox = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(
                    title: "Error",
                    text: "Encountered a problem while saving the file.",
                    icon: Icon.Error
                );
                await messageBox.Show().ConfigureAwait(false);
            }
        }

        public void Start()
        {
#if DEBUG
            var streamWriter = StreamWriter.Null;
#endif
            ArtAlg.Program.ProcessEndedCallback = () =>
            {
                StartButtonActive = true;
                StopButtonActive = false;
                FilepathSourceButtonActive = true;
                ArtAlg.Program.TokenSource.Dispose();
#if DEBUG
                streamWriter.Dispose();
#endif
            };
            ArtAlg.Program.ImageUpdateCallback = () =>
            {
                PropertyChangedInvoke(nameof(Image));
                PropertyChangedInvoke(nameof(Timer));
                PropertyChangedInvoke(nameof(PolygonCount));
                PropertyChangedInvoke(nameof(PointCount));
                PropertyChangedInvoke(nameof(MeanPixelError));
                PropertyChangedInvoke(nameof(PSNR));
                PropertyChangedInvoke(nameof(SSIM));
                PropertyChangedInvoke(nameof(Generation));
            };
#if DEBUG
            ArtAlg.Program.TaskRun(streamWriter);
#else
            ArtAlg.Program.TaskRun(null);
#endif
            StartButtonActive = false;
            StopButtonActive = true;
            FilepathSourceButtonActive = false;
            FilepathResultButtonActive = true;
        }

        public static void Stop() => ArtAlg.Program.TokenSource.Cancel();

        public static void Help()
        {
            var messageBox = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxCustomWindow(
                new MessageBox.Avalonia.DTO.MessageBoxCustomParams()
                {
                    ContentTitle = "Help",
                    ContentMessage =
                    "Evolutionary Image Replicator is an example usage of a genetic algorithm.\n\n" +
                    "How to start?\n" +
                    "1. Load an image that you want to replicate\n" +
                    "2. Click the play button to start the evolution\n\n" +
                    "Above the play button you can find a field with number of generations to\n" +
                    "run the algorithm for. The higher it is the longer it takes, but the result\n" +
                    "will get better. If you get bored in the meantime you can stop the process\n" +
                    "anytime you like with a stop button.\n\n" +
                    "You can save the currently displayed picture with a proper button located\n" +
                    "just below the button you loaded your image with.\n\n" +
                    "If you want to get more control over the algorithm you can fiddle with\n" +
                    "settings by opening a sidebar with a button on the right edge. When you\n" +
                    "get lost just click the reset button.\n\n" +
                    "Have fun! :)"
                }
            );
            messageBox.Show();
        }

        public void SwitchSettingsSidebar()
        {
            SettingsSidebarActive = !SettingsSidebarActive;
            PropertyChangedInvoke(nameof(Arrow));
        }
    }
}
