﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Gui
{
    internal class Settings : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void PropertyChangedInvoke([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        internal ulong GenerationMax
        {
            get => ArtAlg.Settings.GenerationMax;
            set
            {
                ArtAlg.Settings.GenerationMax = value;
                PropertyChangedInvoke();
            }
        }

        #region shape limits
        internal ushort PointsMin
        {
            get => ArtAlg.Settings.PointsMin;
            set
            {
                ArtAlg.Settings.PointsMin = value;
                PropertyChangedInvoke();
            }
        }
        internal ushort PointsMax
        {
            get => ArtAlg.Settings.PointsMax;
            set
            {
                ArtAlg.Settings.PointsMax = value;
                PropertyChangedInvoke();
            }
        }
        internal ushort PointsPerPolygonMin
        {
            get => ArtAlg.Settings.PointsPerPolygonMin;
            set
            {
                ArtAlg.Settings.PointsPerPolygonMin = value;
                PropertyChangedInvoke();
            }
        }
        internal ushort PointsPerPolygonMax
        {
            get => ArtAlg.Settings.PointsPerPolygonMax;
            set
            {
                ArtAlg.Settings.PointsPerPolygonMax = value;
                PropertyChangedInvoke();
            }
        }
        internal ushort PolygonsMin
        {
            get => ArtAlg.Settings.PolygonsMin;
            set
            {
                ArtAlg.Settings.PolygonsMin = value;
                PropertyChangedInvoke();
            }
        }
        internal ushort PolygonsMax
        {
            get => ArtAlg.Settings.PolygonsMax;
            set
            {
                ArtAlg.Settings.PolygonsMax = value;
                PropertyChangedInvoke();
            }
        }
        #endregion shape limits

        #region mutation rates
        internal float ColorMutationRate
        {
            get => ArtAlg.Settings.ColorMutationRate;
            set
            {
                ArtAlg.Settings.ColorMutationRate = value;
                PropertyChangedInvoke();
            }
        }

        internal float AddPointMutationRate
        {
            get => ArtAlg.Settings.AddPointMutationRate;
            set
            {
                ArtAlg.Settings.AddPointMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float RemovePointMutationRate
        {
            get => ArtAlg.Settings.RemovePointMutationRate;
            set
            {
                ArtAlg.Settings.RemovePointMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float MovePointMinMutationRate
        {
            get => ArtAlg.Settings.MovePointMinMutationRate;
            set
            {
                ArtAlg.Settings.MovePointMinMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float MovePointMidMutationRate
        {
            get => ArtAlg.Settings.MovePointMidMutationRate;
            set
            {
                ArtAlg.Settings.MovePointMidMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float MovePointMaxMutationRate
        {
            get => ArtAlg.Settings.MovePointMaxMutationRate;
            set
            {
                ArtAlg.Settings.MovePointMaxMutationRate = value;
                PropertyChangedInvoke();
            }
        }

        internal float AddPolygonMutationRate
        {
            get => ArtAlg.Settings.AddPolygonMutationRate;
            set
            {
                ArtAlg.Settings.AddPolygonMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float RemovePolygonMutationRate
        {
            get => ArtAlg.Settings.RemovePolygonMutationRate;
            set
            {
                ArtAlg.Settings.RemovePolygonMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        internal float MovePolygonMutationRate
        {
            get => ArtAlg.Settings.MovePolygonMutationRate;
            set
            {
                ArtAlg.Settings.MovePolygonMutationRate = value;
                PropertyChangedInvoke();
            }
        }
        #endregion mutation rates

        #region color limits
        internal byte ColorRangeMin
        {
            get => ArtAlg.Settings.ColorRangeMin;
            set
            {
                ArtAlg.Settings.ColorRangeMin = value;
                PropertyChangedInvoke();
            }
        }
        internal byte ColorRangeMax
        {
            get => ArtAlg.Settings.ColorRangeMax;
            set
            {
                ArtAlg.Settings.ColorRangeMax = value;
                PropertyChangedInvoke();
            }
        }
        internal byte AlphaRangeMin
        {
            get => ArtAlg.Settings.AlphaRangeMin;
            set
            {
                ArtAlg.Settings.AlphaRangeMin = value;
                PropertyChangedInvoke();
            }
        }
        internal byte AlphaRangeMax
        {
            get => ArtAlg.Settings.AlphaRangeMax;
            set
            {
                ArtAlg.Settings.AlphaRangeMax = value;
                PropertyChangedInvoke();
            }
        }
        #endregion color limits

        internal void ResetSettings()
        {
            ArtAlg.Settings.ResetSettings();

            PropertyChangedInvoke(nameof(GenerationMax));

            PropertyChangedInvoke(nameof(PointsMin));
            PropertyChangedInvoke(nameof(PointsMax));
            PropertyChangedInvoke(nameof(PointsPerPolygonMin));
            PropertyChangedInvoke(nameof(PointsPerPolygonMax));
            PropertyChangedInvoke(nameof(PolygonsMin));
            PropertyChangedInvoke(nameof(PolygonsMax));

            PropertyChangedInvoke(nameof(ColorMutationRate));
            PropertyChangedInvoke(nameof(AddPointMutationRate));
            PropertyChangedInvoke(nameof(RemovePointMutationRate));
            PropertyChangedInvoke(nameof(MovePointMinMutationRate));
            PropertyChangedInvoke(nameof(MovePointMidMutationRate));
            PropertyChangedInvoke(nameof(MovePointMaxMutationRate));
            PropertyChangedInvoke(nameof(AddPolygonMutationRate));
            PropertyChangedInvoke(nameof(RemovePolygonMutationRate));
            PropertyChangedInvoke(nameof(MovePolygonMutationRate));

            PropertyChangedInvoke(nameof(ColorRangeMin));
            PropertyChangedInvoke(nameof(ColorRangeMax));
            PropertyChangedInvoke(nameof(AlphaRangeMin));
            PropertyChangedInvoke(nameof(AlphaRangeMax));
        }
    }
}
