﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewLocator.Build(System.Object)~Avalonia.Controls.IControl")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewLocator.Build(System.Object)~Avalonia.Controls.IControl")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~P:Gui.ViewModels.MainWindowViewModel.MeanPixelError")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~P:Gui.ViewModels.MainWindowViewModel.Timer")]
[assembly: SuppressMessage("AsyncUsage", "AsyncFixer03:Fire & forget async void methods", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewModels.MainWindowViewModel.LoadImage")]
[assembly: SuppressMessage("AsyncUsage", "AsyncFixer03:Fire & forget async void methods", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewModels.MainWindowViewModel.SaveImage")]
[assembly: SuppressMessage("AsyncUsage", "AsyncFixer03:Fire & forget async void methods", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewModels.MainWindowViewModel.Help")]
[assembly: SuppressMessage("Critical Code Smell", "S927:parameter names should match base declaration and other partial definitions", Justification = "<Pending>", Scope = "member", Target = "~M:Gui.ViewLocator.Build(System.Object)~Avalonia.Controls.IControl")]
