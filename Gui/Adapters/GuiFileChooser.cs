﻿using Avalonia.Controls;
using System.Threading.Tasks;

namespace Gui.Adapters
{
    internal class GuiFileChooser : IFileChooser
    {
        private readonly Window parent;

        internal GuiFileChooser(Window parent)
        {
            this.parent = parent;
        }

        public async Task<string> OpenFileDialog()
        {
            var dialog = new OpenFileDialog();
            var result = await dialog.ShowAsync(parent).ConfigureAwait(false);

            if (result != null)
            {
                return string.Join(" ", result);
            }

            return null;
        }

        public async Task<string> SaveFileDialog()
        {
            var dialog = new SaveFileDialog();
            var result = await dialog.ShowAsync(parent).ConfigureAwait(false);

            if (result != null)
            {
                return string.Join(" ", result);
            }

            return null;
        }
    }
}
