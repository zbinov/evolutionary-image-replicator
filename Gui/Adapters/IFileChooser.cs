﻿using System.Threading.Tasks;

namespace Gui.Adapters
{
    internal interface IFileChooser
    {
        public Task<string> OpenFileDialog();

        public Task<string> SaveFileDialog();
    }
}
