﻿using ArtAlg.Graphics;
using static ArtAlg.Graphics.ExtensionMethods;

namespace ArtAlg
{
    static class Renderer
    {
        internal static void Render(Canvas image, System.Drawing.Graphics graphics)
        {
            graphics.Clear(System.Drawing.Color.Gray);

            foreach (var polygon in image.Polygons)
            {
                Render(polygon, graphics);
            }
        }

        private static void Render(Polygon polygon, System.Drawing.Graphics graphics)
        {
            using var brush = polygon.Color.ToSystemBrush();
            graphics.FillPolygon(brush, polygon.Points.ToSystemPoints());
        }
    }
}
