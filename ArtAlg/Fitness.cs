﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using static ArtAlg.Utils;

namespace ArtAlg
{
    class Fitness : IDisposable
    {
        private Bitmap SourceImage { get; }
        private BitmapData SourceData { get; }

        private bool disposed;

        public Fitness(Bitmap sourceImage)
        {
            SourceImage = sourceImage;

            SourceData = SourceImage.LockBits(
                new Rectangle(0, 0, SourceImage.Width, SourceImage.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb
            );
        }

        internal unsafe long MPE(Graphics.Canvas imageC)
        {
            var error = 0L;

            using var image = new Bitmap(MaxWidth, MaxHeight, PixelFormat.Format24bppRgb);
            using (var graphics = System.Drawing.Graphics.FromImage(image))
            {
                Renderer.Render(imageC, graphics);
            }
            var imageData = image.LockBits(
                new Rectangle(0, 0, MaxWidth, MaxHeight),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb
            );

            for (ushort y = 0; y < MaxHeight; y++)
            {
                for (ushort x = 0; x < MaxWidth; x++)
                {
                    var ip = (byte*)imageData.Scan0 + y * imageData.Stride + 3 * x;
                    var sp = (byte*)SourceData.Scan0 + y * SourceData.Stride + 3 * x;
                    var r = ip[2] - sp[2];
                    var g = ip[1] - sp[1];
                    var b = ip[0] - sp[0];
                    error += r * r + g * g + b * b;
                }
            }

            image.UnlockBits(imageData);

            return error;
        }

        // ssim based on https://github.com/x-stars/ImageQuality/blob/master/ImageQuality/Models/ImagePair.cs
        internal unsafe (long, double) MPEandSSIM(Graphics.Canvas imageC)
        {
            var error = 0L;

            var whc = MaxWidth * MaxHeight * 3;
            const float k1 = 0.01f;
            const float k2 = 0.03f;
            const float c1 = k1 * k1 * 255 * 255;
            const float c2 = k2 * k2 * 255 * 255;
            const float c3 = c2 / 2;

            var iMu = 0f;
            var sMu = 0f;
            var iVarSum = 0f;
            var sVarSum = 0f;
            var covSum = 0f;

            using (var image = new Bitmap(MaxWidth, MaxHeight, PixelFormat.Format24bppRgb))
            {
                using (var graphics = System.Drawing.Graphics.FromImage(image))
                {
                    Renderer.Render(imageC, graphics);
                }
                var imageData = image.LockBits(
                    new Rectangle(0, 0, MaxWidth, MaxHeight),
                    ImageLockMode.ReadOnly,
                    PixelFormat.Format24bppRgb
                );

                for (ushort y = 0; y < MaxHeight; y++)
                {
                    for (ushort x = 0; x < MaxWidth; x++)
                    {
                        var ip = (byte*)imageData.Scan0 + y * imageData.Stride + 3 * x;
                        var sp = (byte*)SourceData.Scan0 + y * SourceData.Stride + 3 * x;
                        var r = ip[2] - sp[2];
                        var g = ip[1] - sp[1];
                        var b = ip[0] - sp[0];
                        error += r * r + g * g + b * b;
                    }
                }

                for (ushort y = 0; y < MaxHeight; y++)
                {
                    for (ushort x = 0; x < MaxWidth; x++)
                    {
                        var ip = (byte*)imageData.Scan0 + y * imageData.Stride + 3 * x;
                        var sp = (byte*)SourceData.Scan0 + y * SourceData.Stride + 3 * x;
                        iMu += ip[2] + ip[1] + ip[0];
                        sMu += sp[2] + sp[1] + sp[0];
                    }
                }
                iMu /= whc;
                sMu /= whc;

                for (ushort y = 0; y < MaxHeight; y++)
                {
                    for (ushort x = 0; x < MaxWidth; x++)
                    {
                        var ip = (byte*)imageData.Scan0 + y * imageData.Stride + 3 * x;
                        var sp = (byte*)SourceData.Scan0 + y * SourceData.Stride + 3 * x;

                        var iErrorR = ip[2] - iMu;
                        var iErrorG = ip[1] - iMu;
                        var iErrorB = ip[0] - iMu;
                        var sErrorR = sp[2] - sMu;
                        var sErrorG = sp[1] - sMu;
                        var sErrorB = sp[0] - sMu;

                        iVarSum += iErrorR * iErrorR + iErrorG * iErrorG + iErrorB * iErrorB;
                        sVarSum += sErrorR * sErrorR + sErrorG * sErrorG + sErrorB * sErrorB;
                        covSum += iErrorR * sErrorR + iErrorG * sErrorG + iErrorB * sErrorB;
                    }
                }

                image.UnlockBits(imageData);
            }

            var iSigma = Math.Sqrt(iVarSum / (whc - 1));
            var sSigma = Math.Sqrt(sVarSum / (whc - 1));
            var isSigma = covSum / (whc - 1);

            var luminance = (2 * iMu * sMu + c1) / (iMu * iMu + sMu * sMu + c1);
            var contrast = (2 * iSigma * iSigma + c2) / (iSigma * iSigma + sSigma * sSigma + c2);
            var structure = (isSigma + c3) / (iSigma * sSigma + c3);

            var ssim = luminance * contrast * structure;

            return (error, ssim);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                SourceImage.UnlockBits(SourceData);
                SourceImage.Dispose();
            }

            disposed = true;
        }
    }
}
