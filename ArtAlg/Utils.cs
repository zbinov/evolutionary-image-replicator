﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ArtAlg
{
    static class Utils
    {
        #region random
        private static readonly Random random = new Random();

        internal static double RandomDouble() =>
            random.NextDouble();

        internal static int RandomInt(int min, int max) =>
            random.Next(min, max);

        internal static byte RandomByte(byte min, byte max) =>
            (byte)random.Next(min, max + 1);
        #endregion random

        internal static ushort MaxWidth { get; set; }
        internal static ushort MaxHeight { get; set; }

        internal static Bitmap CanvasToBitmap(Graphics.Canvas image, int width, int height)
        {
            var bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            using var graphics = System.Drawing.Graphics.FromImage(bitmap);
            Renderer.Render(image, graphics);
            return bitmap;
        }

        internal static void Save(Graphics.Canvas image, int width, int height, string filepath)
        {
            using var bitmap = CanvasToBitmap(image, width, height);
            bitmap.Save(filepath);
        }

        internal static void CleanUpResults(string path, string pattern)
        {
            var dir = new DirectoryInfo(path);

            foreach (var file in dir.EnumerateFiles(pattern))
            {
                file.Delete();
            }
        }
    }
}
