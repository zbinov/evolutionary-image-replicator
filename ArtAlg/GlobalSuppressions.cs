﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S112:General exceptions should never be thrown", Justification = "<Pending>", Scope = "member", Target = "~M:ArtAlg.Graphics.ExtensionMethods.Swap``1(System.Collections.Generic.List{``0},System.Int32,System.Int32)")]
[assembly: SuppressMessage("Design", "RCS1169:Make field read-only.", Justification = "<Pending>", Scope = "member", Target = "~F:ArtAlg.Settings.filepathSource")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<Pending>", Scope = "member", Target = "~F:ArtAlg.Settings.filepathSource")]
[assembly: SuppressMessage("Design", "RCS1158:Static member in generic type should use a type parameter.", Justification = "<Pending>", Scope = "member", Target = "~M:ArtAlg.Graphics.Genetic`1.WillMutate(System.Single)~System.Boolean")]
