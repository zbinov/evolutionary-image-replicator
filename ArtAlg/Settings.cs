﻿using System.IO;
using static ArtAlg.Utils;

namespace ArtAlg
{
    public static class Settings
    {
        private static string filepathSource = "../../../images/!ricky.png";
#if DEBUG
        internal const string filepathResultDebug = "../../../images/result_";
#endif

        public static string FilepathSource { get; set; } = filepathSource;
        public static string FilepathResult { get; set; }
        public static string FileExtension => Path.GetExtension(FilepathSource);

        public static ulong GenerationMax { get; set; } = 100_000;

        #region default values
        private const ushort pointsMinDefault = 0;
        private const ushort pointsMaxDefault = 3_000;
        private const ushort pointsPerPolygonMinDefault = 3;
        private const ushort pointsPerPolygonMaxDefault = 10;
        private const ushort polygonsMinDefault = 0;
        private const ushort polygonsMaxDefault = 500;

        private const float colorMutationRateDefault = 0.003f;

        private const float addPointMutationRateDefault = 0.003f;
        private const float removePointMutationRateDefault = 0.003f;
        private const float movePointMinMutationRateDefault = 0.002f;
        private const float movePointMidMutationRateDefault = 0.003f;
        private const float movePointMaxMutationRateDefault = 0.001f;

        private const float addPolygonMutationRateDefault = 0.007f;
        private const float removePolygonMutationRateDefault = 0.007f;
        private const float movePolygonMutationRateDefault = 0.007f;

        private const byte colorRangeMinDefault = 0;
        private const byte colorRangeMaxDefault = 255;
        private const byte alphaRangeMinDefault = 50;
        private const byte alphaRangeMaxDefault = 150;
        #endregion default values

        #region shape limits
        public static ushort PointsMin { get; set; } = pointsMinDefault;
        public static ushort PointsMax { get; set; } = pointsMaxDefault;
        public static ushort PointsPerPolygonMin { get; set; } = pointsPerPolygonMinDefault;
        public static ushort PointsPerPolygonMax { get; set; } = pointsPerPolygonMaxDefault;
        public static ushort PolygonsMin { get; set; } = polygonsMinDefault;
        public static ushort PolygonsMax { get; set; } = polygonsMaxDefault;
        #endregion shape limits

        #region mutation rates
        public static float ColorMutationRate { get; set; } = colorMutationRateDefault;

        public static float AddPointMutationRate { get; set; } = addPointMutationRateDefault;
        public static float RemovePointMutationRate { get; set; } = removePointMutationRateDefault;
        public static float MovePointMinMutationRate { get; set; } = movePointMinMutationRateDefault;
        public static float MovePointMidMutationRate { get; set; } = movePointMidMutationRateDefault;
        public static float MovePointMaxMutationRate { get; set; } = movePointMaxMutationRateDefault;

        public static float AddPolygonMutationRate { get; set; } = addPolygonMutationRateDefault;
        public static float RemovePolygonMutationRate { get; set; } = removePolygonMutationRateDefault;
        public static float MovePolygonMutationRate { get; set; } = movePolygonMutationRateDefault;
        #endregion mutation rates

        #region range limits
        public static byte ColorRangeMin { get; set; } = colorRangeMinDefault;
        public static byte ColorRangeMax { get; set; } = colorRangeMaxDefault;
        public static byte AlphaRangeMin { get; set; } = alphaRangeMinDefault;
        public static byte AlphaRangeMax { get; set; } = alphaRangeMaxDefault;
        public static ushort MovePointRangeMin => (ushort)(0.03 * 0.5 * (MaxWidth + MaxHeight));
        public static ushort MovePointRangeMid => (ushort)(0.3 * 0.5 * (MaxWidth + MaxHeight));
        #endregion range limits

        public static void ResetSettings()
        {
            PointsMin = pointsMinDefault;
            PointsMax = pointsMaxDefault;
            PointsPerPolygonMin = pointsPerPolygonMinDefault;
            PointsPerPolygonMax = pointsPerPolygonMaxDefault;
            PolygonsMin = polygonsMinDefault;
            PolygonsMax = polygonsMaxDefault;

            ColorMutationRate = colorMutationRateDefault;
            AddPointMutationRate = addPointMutationRateDefault;
            RemovePointMutationRate = removePointMutationRateDefault;
            MovePointMinMutationRate = movePointMinMutationRateDefault;
            MovePointMidMutationRate = movePointMidMutationRateDefault;
            MovePointMaxMutationRate = movePointMaxMutationRateDefault;
            AddPolygonMutationRate = addPolygonMutationRateDefault;
            RemovePolygonMutationRate = removePolygonMutationRateDefault;
            MovePolygonMutationRate = movePolygonMutationRateDefault;

            ColorRangeMin = colorRangeMinDefault;
            ColorRangeMax = colorRangeMaxDefault;
            AlphaRangeMin = alphaRangeMinDefault;
            AlphaRangeMax = alphaRangeMaxDefault;
        }
    }
}
