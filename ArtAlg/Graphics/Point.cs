﻿using static ArtAlg.Settings;
using static ArtAlg.Utils;
using static System.Math;

namespace ArtAlg.Graphics
{
    class Point : Genetic<Point>
    {
        internal int X { get; private set; }
        internal int Y { get; private set; }

        #region constructors
        internal Point()
        {
            X = RandomInt(0, MaxWidth + 1);
            Y = RandomInt(0, MaxHeight + 1);
        }

        internal Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        #endregion constructors

        internal override Point Copy() => new Point(X, Y);

        internal void Mutate(Canvas canvas)
        {
            if (WillMutate(MovePointMaxMutationRate))
            {
                X = RandomInt(0, MaxWidth + 1);
                Y = RandomInt(0, MaxHeight + 1);
                canvas.SetDirty();
            }

            if (WillMutate(MovePointMidMutationRate))
            {
                X = Min(Max(0, X + RandomInt(-MovePointRangeMid, MovePointRangeMid + 1)), MaxWidth);
                Y = Min(Max(0, Y + RandomInt(-MovePointRangeMid, MovePointRangeMid + 1)), MaxHeight);
                canvas.SetDirty();
            }

            if (WillMutate(MovePointMinMutationRate))
            {
                X = Min(Max(0, X + RandomInt(-MovePointRangeMin, MovePointRangeMin + 1)), MaxWidth);
                Y = Min(Max(0, Y + RandomInt(-MovePointRangeMin, MovePointRangeMin + 1)), MaxHeight);
                canvas.SetDirty();
            }
        }
    }
}
