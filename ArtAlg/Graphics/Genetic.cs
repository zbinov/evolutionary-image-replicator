﻿namespace ArtAlg.Graphics
{
    abstract class Genetic<T>
    {
        internal static bool WillMutate(float mutationRate) =>
            Utils.RandomDouble() <= mutationRate;

        abstract internal T Copy();
    }
}
