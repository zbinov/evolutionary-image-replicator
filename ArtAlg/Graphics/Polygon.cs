﻿using System.Collections.Generic;
using static ArtAlg.Settings;
using static ArtAlg.Utils;
using static System.Math;

namespace ArtAlg.Graphics
{
    class Polygon : Genetic<Polygon>
    {
        internal List<Point> Points { get; }
        internal Color Color { get; }

        #region constructors
        internal Polygon()
        {
            Points = new List<Point>(PointsPerPolygonMin);

            var starterPoint = new Point();

            for (int i = 0; i < PointsPerPolygonMin; i++)
            {
                Points.Add(new Point(
                    x: Min(Max(0, starterPoint.X + RandomInt(-MovePointRangeMin, MovePointRangeMin + 1)), MaxWidth),
                    y: Min(Max(0, starterPoint.Y + RandomInt(-MovePointRangeMin, MovePointRangeMin + 1)), MaxHeight)
                ));
            }

            Color = new Color();
        }

        internal Polygon(List<Point> points, Color color)
        {
            Points = points;
            Color = color;
        }
        #endregion constructors

        internal override Polygon Copy() => new Polygon(Points.Copy(), Color.Copy());

        internal void Mutate(Canvas canvas)
        {
            if (WillMutate(RemovePointMutationRate))
            {
                RemovePoint(canvas);
            }

            if (WillMutate(AddPointMutationRate))
            {
                AddPoint(canvas);
            }

            foreach (var point in Points)
            {
                point.Mutate(canvas);
            }

            Color.Mutate(canvas);
        }

        private void RemovePoint(Canvas canvas)
        {
            if (Points.Count > PointsPerPolygonMin && canvas.PointCount > PointsMin)
            {
                Points.RemoveAt(RandomInt(0, Points.Count));
                canvas.SetDirty();
            }
        }

        private void AddPoint(Canvas canvas)
        {
            if (Points.Count < PointsPerPolygonMax && canvas.PointCount < PointsMax)
            {
                int index = RandomInt(1, Points.Count - 1);
                Points.Insert(
                    index,
                    new Point(
                        (Points[index - 1].X + Points[index].X) / 2,
                        (Points[index - 1].Y + Points[index].Y) / 2
                    )
                );
                canvas.SetDirty();
            }
        }
    }
}
