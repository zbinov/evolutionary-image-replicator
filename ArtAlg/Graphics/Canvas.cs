﻿using System.Collections.Generic;
using static ArtAlg.Settings;
using static ArtAlg.Utils;

namespace ArtAlg.Graphics
{
    class Canvas : Genetic<Canvas>
    {
        internal List<Polygon> Polygons { get; }
        internal bool IsDirty { get; private set; }

        internal int PointCount
        {
            get
            {
                int sum = 0;
                foreach (var polygon in Polygons)
                {
                    sum += polygon.Points.Count;
                }
                return sum;
            }
        }

        internal void SetDirty() => IsDirty = true;

        #region constructors
        internal Canvas()
        {
            Polygons = new List<Polygon>(PolygonsMin);

            for (int i = 0; i < PolygonsMin; i++)
            {
                AddPolygon();
            }

            SetDirty();
        }

        internal Canvas(List<Polygon> polygons)
        {
            Polygons = polygons;
        }
        #endregion constructors

        internal override Canvas Copy() => new Canvas(Polygons.Copy());

        internal void Mutate()
        {
            if (WillMutate(RemovePolygonMutationRate))
            {
                RemovePolygon();
            }

            if (WillMutate(AddPolygonMutationRate))
            {
                AddPolygon();
            }

            if (WillMutate(MovePolygonMutationRate))
            {
                MovePolygon();
            }

            foreach (var polygon in Polygons)
            {
                polygon.Mutate(this);
            }
        }

        private void AddPolygon()
        {
            if (Polygons.Count < PolygonsMax && PointCount + PointsPerPolygonMin <= PointsMax)
            {
                Polygons.Insert(
                    index: RandomInt(0, Polygons.Count),
                    item: new Polygon()
                );
                SetDirty();
            }
        }

        private void RemovePolygon()
        {
            if (Polygons.Count > PolygonsMin)
            {
                int index = RandomInt(0, Polygons.Count);
                if (PointCount - Polygons[index].Points.Count >= PointsMin)
                {
                    Polygons.RemoveAt(index);
                }
                SetDirty();
            }
        }

        private void MovePolygon()
        {
            if (Polygons.Count > 1)
            {
                Polygons.Swap(
                    indexFrom: RandomInt(0, Polygons.Count),
                    indexTo: RandomInt(0, Polygons.Count)
                );
                SetDirty();
            }
        }
    }
}
