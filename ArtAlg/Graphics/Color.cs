﻿using static ArtAlg.Settings;
using static ArtAlg.Utils;

namespace ArtAlg.Graphics
{
    class Color : Genetic<Color>
    {
        internal byte Red { get; private set; }
        internal byte Green { get; private set; }
        internal byte Blue { get; private set; }
        internal byte Alpha { get; private set; }

        #region constructors
        internal Color()
        {
            Red = RandomByte(ColorRangeMin, ColorRangeMax);
            Green = RandomByte(ColorRangeMin, ColorRangeMax);
            Blue = RandomByte(ColorRangeMin, ColorRangeMax);
            Alpha = RandomByte(AlphaRangeMin, AlphaRangeMax);
        }

        internal Color(byte r, byte g, byte b, byte a)
        {
            Red = r;
            Green = g;
            Blue = b;
            Alpha = a;
        }
        #endregion constructors

        internal override Color Copy() => new Color(Red, Green, Blue, Alpha);

        internal void Mutate(Canvas canvas)
        {
            if (WillMutate(ColorMutationRate))
            {
                Red = RandomByte(ColorRangeMin, ColorRangeMax);
                canvas.SetDirty();
            }

            if (WillMutate(ColorMutationRate))
            {
                Green = RandomByte(ColorRangeMin, ColorRangeMax);
                canvas.SetDirty();
            }

            if (WillMutate(ColorMutationRate))
            {
                Blue = RandomByte(ColorRangeMin, ColorRangeMax);
                canvas.SetDirty();
            }

            if (WillMutate(ColorMutationRate))
            {
                Alpha = RandomByte(AlphaRangeMin, AlphaRangeMax);
                canvas.SetDirty();
            }
        }
    }
}
