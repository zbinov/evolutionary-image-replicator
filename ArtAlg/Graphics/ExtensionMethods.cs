﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ArtAlg.Graphics
{
    static class ExtensionMethods
    {
        internal static List<T> Copy<T>(this List<T> list) where T : Genetic<T>
        {
            var result = new List<T>(list.Count);

            foreach (var element in list)
            {
                result.Add(element.Copy());
            }

            return result;
        }

        internal static void Swap<T>(this List<T> list, int indexFrom, int indexTo)
        {
            if (indexFrom == indexTo) return;

            if (indexFrom < 0 || indexFrom > list.Count || indexTo < 0 || indexTo > list.Count)
            {
                throw new System.IndexOutOfRangeException("Index out of range was used");
            }

            T item = list[indexFrom];
            list.RemoveAt(indexFrom);

            list.Insert(
                indexTo < indexFrom ? indexTo : indexTo - 1,
                item
            );
        }

        internal static Brush ToSystemBrush(this Color color) =>
            new SolidBrush(System.Drawing.Color.FromArgb(color.Alpha, color.Red, color.Green, color.Blue));

        internal static System.Drawing.Point[] ToSystemPoints(this IList<Point> points) =>
            points.Select(point => new System.Drawing.Point(point.X, point.Y)).ToArray();
    }
}
