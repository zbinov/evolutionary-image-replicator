﻿using ArtAlg.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static ArtAlg.Settings;
using static ArtAlg.Utils;

namespace ArtAlg
{
    public static class Program
    {
        private static Fitness fitness;
        private static double errorCurrent;
        private static double ssimCurrent;
        private static Canvas canvasCurrent;

        public static ulong Generation { get; private set; }
        public static Bitmap Bitmap => CanvasToBitmap(canvasCurrent, MaxWidth, MaxHeight);
        public static int PolygonCount => canvasCurrent?.Polygons.Count ?? 0;
        public static int PointCount => canvasCurrent?.PointCount ?? 0;
        public static double? MeanPixelError
        {
            get
            {
                var result = Math.Sqrt(errorCurrent / (MaxWidth * MaxHeight) / 3);
                return double.IsNaN(result) ? (double?)null : result;
            }
        }
        public static double? PSNR
        {
            get
            {
                var mpe = MeanPixelError;
                return mpe.HasValue ? 10 * Math.Log10(255 * 255 / mpe.Value) : (double?)null;
            }
        }
        public static double? SSIM => double.IsNaN(ssimCurrent) ? (double?)null : ssimCurrent;

        public static readonly Stopwatch stopwatch = new();
        private static StreamWriter StreamWriter { get; set; }

        #region gui related
        public static Action ProcessEndedCallback { get; set; }
        public static Action ImageUpdateCallback { get; set; }

        public static CancellationTokenSource TokenSource { get; private set; }
        private static CancellationToken CancellationToken { get; set; }
        #endregion gui related

        public static void Main()
        {
            using var streamWriter = new StreamWriter(Console.OpenStandardOutput())
            {
                AutoFlush = false
            };
            PreSetup(streamWriter);

            Evolve();
        }

        public static void TaskRun(StreamWriter streamWriter)
        {
            PreSetup(streamWriter);

            TokenSource = new CancellationTokenSource();
            CancellationToken = TokenSource.Token;

            Task.Run(Evolve, CancellationToken);
        }

        private static void PreSetup(StreamWriter streamWriter)
        {
#if DEBUG
            CleanUpResults(Path.GetDirectoryName(filepathResultDebug), Path.GetFileName(filepathResultDebug) + "*");
#endif
            using (var imageSource = Image.FromFile(FilepathSource))
            {
                fitness?.Dispose();
                fitness = new Fitness(new Bitmap(Image.FromFile(FilepathSource)));
                MaxWidth = (ushort)imageSource.Width;
                MaxHeight = (ushort)imageSource.Height;
            }

            errorCurrent = double.MaxValue;
            ssimCurrent = 0f;
            canvasCurrent = new Canvas();
            Generation = 0;
            stopwatch.Reset();

            StreamWriter = streamWriter;
        }

        private static void Evolve()
        {
            stopwatch.Start();
            while (Generation < GenerationMax)
            {
                if (CancellationToken.IsCancellationRequested)
                {
                    ProcessEndedCallback?.Invoke();
                    ImageUpdateCallback?.Invoke();
                    CancellationToken.ThrowIfCancellationRequested();
                }

                var canvasNew = canvasCurrent.Copy();
                canvasNew.Mutate();

                ////if (true)
                if (canvasNew.IsDirty)
                {
                    Generation++;

                    var errorNew = fitness.MPE(canvasNew);
                    ////(var errorNew, var ssimNew) = fitness.MPEandSSIM(canvasNew);

                    if (errorNew <= errorCurrent)
                    ////if (ssimCurrent < 0.6f
                    ////    ? errorNew <= errorCurrent
                    ////    : errorNew <= errorCurrent && ssimNew >= ssimCurrent - 0.05 * ssimCurrent)
                    ////if (errorNew <= errorCurrent && ssimNew >= ssimCurrent)
                    {
                        canvasCurrent = canvasNew;
                        errorCurrent = errorNew;
                        ////ssimCurrent = ssimNew;

                        ImageUpdateCallback?.Invoke();
                    }

                    #region output
                    if (StreamWriter != null)
                    {
                        var checkpoints = new List<Tuple<uint, uint>>
                        {
                            new Tuple<uint, uint>(1000, 10000),
                            new Tuple<uint, uint>(100, 1000),
                            new Tuple<uint, uint>(10, 100)
                        };
                        foreach (var checkpoint in checkpoints)
                        {
                            if (Generation < GenerationMax / checkpoint.Item1 && Generation % (float)(GenerationMax / checkpoint.Item2) == 0)
                            {
                                OutputResults();
                            }
                            else if (checkpoint == checkpoints.Last())
                            {
                                if (Generation % (float)(GenerationMax / 10) == 0)
                                {
                                    OutputResults();
                                }
                                else if (Generation == GenerationMax)
                                {
                                    OutputResults();
                                }
                            }
                        }
                    }
                    #endregion output
                }
            }
            stopwatch.Stop();
            ImageUpdateCallback?.Invoke();
            ProcessEndedCallback?.Invoke();
        }

        private static void OutputResults()
        {
            StreamWriter.WriteLine($" genarations: {Generation,16}");
            StreamWriter.WriteLine($" error:       {errorCurrent,16}");
            StreamWriter.WriteLine($" polygons:    {canvasCurrent.Polygons.Count,16}");
            StreamWriter.WriteLine($" points:      {canvasCurrent.PointCount,16}");
            StreamWriter.WriteLine($" time:        {stopwatch.Elapsed,16}");
            StreamWriter.WriteLine("".PadRight(30, '-'));
            StreamWriter.Flush();
#if DEBUG
            Save(
                canvasCurrent,
                MaxWidth,
                MaxHeight,
                filepathResultDebug + "_" + Generation + "_" + errorCurrent + FileExtension
            );
#endif
        }
    }
}
